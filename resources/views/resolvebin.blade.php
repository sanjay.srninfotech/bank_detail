<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<style>

form#binForm {
    width: 34%;
    /* justify-content: center; */
    margin: auto;
    padding: 50px;
}
    </style>
<body>
    <div class="container">

<div id="result">
    <form id="binForm">
        <div class="col-12">
            <label for="bin">BIN Code:</label>
            <input type="text" class="form-control" name="bin" id="bin" required>
        </div>
        <div class="col-6 mt-2">
            <button type="submit" class="btn btn-primary">Get Details</button>
        </div>
    </form>
</div>
    </div>
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<script>
$(document).ready(function () {
    var apiEndpoint = 'https://api.paystack.co/decision/';
    var secretKey = 'YOUR_ACTUAL_SECRET_KEY';

    $('#binForm').submit(function (event) {
        event.preventDefault();

        var binCode = $('#bin').val();

        $.ajax({
            url: apiEndpoint + 'bin/' + binCode,
            type: 'GET',
            headers: {
                'Authorization': 'Bearer ' + secretKey
            },
            success: function (response) {
                if (response.status) {
                    var data = response.data;
                    var resultHtml = `
                        <p>Bin: ${data.bin}</p>
                        <p>Brand: ${data.brand}</p>
                        <p>Country Code: ${data.country_code}</p>
                        <p>Country Name: ${data.country_name}</p>
                        <p>Card Type: ${data.card_type}</p>
                        <p>Bank: ${data.bank}</p>
                        <p>Linked Bank ID: ${data.linked_bank_id}</p>
                    `;
                    $('#result').html(resultHtml);

                    // Display success alert
                    alert('API request successful!');
                } else {
                    $('#result').html('<p>Error: ' + response.message + '</p>');
                }
            },
            error: function (error) {
                $('#result').html('<p>Error: Unable to connect to the API</p>');
            }
        });
    });
});
</script>

</body>
</html>
