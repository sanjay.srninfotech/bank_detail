
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>


    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>

<div class="container">
        <div class="row align-items-center">
            <div class="col-auto">
                <h1 class="text-center">Bank  Details</h1>
            </div>

            <br>

            <div class="card">
                <form id="bankDetailForm">
                    <div class="card">
                        <div class="card-header row gutters-5">
                            <div class="col">
                                <h5 class="mb-md-0 h6">Check Bank Detail:</h5>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="controller">
                                <div class="row">
                                    <div class="col-6">
                                        <label for="bank_code">Bank Code:</label>
                                        <input type="text" class="form-control" name="bank_code" id="bank_code" required>
                                    </div>

                                    <div class="col-6">
                                        <label for="country_code">Country Code:</label>
                                        <input type="text" class="form-control" name="country_code" id="country_code"
                                            required>
                                    </div>

                                    <div class="col-6">
                                        <label for="account_number">Account Number:</label>
                                        <input type="text" class="form-control" name="account_number" id="account_number"
                                            required>
                                    </div>

                                    <div class="col-6">
                                        <label for="account_name">Account Name:</label>
                                        <input type="text" class="form-control" name="account_name" id="account_name"
                                            required>
                                    </div>

                                    <div class="col-6">
                                        <label for="account_type">Account Type:</label>
                                        <input type="text" class="form-control" name="account_type" id="account_type"
                                            required>
                                    </div>

                                    <div class="col-6">
                                        <label for="document_type">Document Type:</label>
                                        <input type="text" class="form-control" name="document_type" id="document_type"
                                            required>
                                    </div>

                                    <div class="col-6">
                                        <label for="document_number">Document Number:</label>
                                        <input type="text" class="form-control" name="document_number"
                                            id="document_number" required>
                                    </div>
                                </div>

                                <button type="button" class="btn btn-primary m-3 float-right"
                                    onclick="validateBankDetails()">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

</div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script>
        const paystackApiKey = 'sk_test_0fc1de922a8023e38f90fbcfcd48e74825680638';

        function validateBankDetails() {
            const bankCode = document.getElementById('bank_code').value;
            const countryCode = document.getElementById('country_code').value;
            const accountNumber = document.getElementById('account_number').value;
            const accountName = document.getElementById('account_name').value;
            const accountType = document.getElementById('account_type').value;
            const documentType = document.getElementById('document_type').value;
            const documentNumber = document.getElementById('document_number').value;

            const requestData = {
                "bank_code": bankCode,
                "country_code": countryCode,
                "account_number": accountNumber,
                "account_name": accountName,
                "account_type": accountType,
                "document_type": documentType,
                "document_number": documentNumber
            };

            fetch('https://api.paystack.co/bank/validate', {
                    method: 'POST',
                    headers: {
                        'Authorization': `Bearer ${paystackApiKey}`,
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(requestData)
                })
                .then(response => response.json())
                .then(data => {
                    console.log('API Response:', data);

                    alert('successfully: ' + data.status);
                })
                .catch(error => {
                    console.error('Error:', error);
                });
        }
    </script>
<body>
</html>

