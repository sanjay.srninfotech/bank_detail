<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>
<body>
<div class="container">

    <div class="aiz-titlebar text-left mt-2 mb-3">
        <div class="row align-items-center">
            <div class="col-auto">
                <h1 class="text-center">customer  Details</h1>
            </div>
        </div>
    </div>

    <div class="card">
        <form id="bankDetailForm">
            <div class="card-header row gutters-5">
                <div class="col">
                    <h5 class="mb-md-0 h6">Check customer Detail:</h5>
                </div>
            </div>
            <div class="card-body">
                <div class="controller">
                    <div class="row">
                        <div class="col-6">
                            <label for="country">Country:</label>
                            <input type="text" class="form-control" name="country" id="country" required>
                        </div>
                        <div class="col-6">
                            <label for="type">Type:</label>
                            <input type="text" class="form-control" name="type" id="type" required>
                        </div>
                        <div class="col-6">
                            <label for="bank_code">Bank Code:</label>
                            <input type="text" class="form-control" name="bank_code" id="bank_code" required>
                        </div>
                        <div class="col-6">
                            <label for="bvn">BVn Code:</label>
                            <input type="text" class="form-control" name="bvn" id="bvn" required>
                        </div>
                        <div class="col-6">
                            <label for="account_number">Account Number:</label>
                            <input type="text" class="form-control" name="account_number" id="account_number" required>
                        </div>
                        <div class="col-6">
                            <label for="first_name">First Name:</label>
                            <input type="text" class="form-control" name="first_name" id="first_name" required>
                        </div>
                        <div class="col-6">
                            <label for="last_name">Last Name:</label>
                            <input type="text" class="form-control" name="last_name" id="last_name" required>
                        </div>
                    </div>

                    <button type="button" class="btn btn-primary m-3 float-right" onclick="validateBankDetails()">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

    <script>
        const paystackApiKey = 'sk_test_0fc1de922a8023e38f90fbcfcd48e74825680638';

        function validateBankDetails() {
            const countryCode = document.getElementById('country').value;
            const type = document.getElementById('type').value;
            const bankCode = document.getElementById('bank_code').value;
            const bvnCode = document.getElementById('bvn').value;
            const accountNumber = document.getElementById('account_number').value;
            const accountName = document.getElementById('first_name').value;
            const accountLastName = document.getElementById('last_name').value;

            const requestData = {
                "country": countryCode,
                "type": type,
                "bank_code": bankCode,
                "bvn": bvnCode,
                "account_number": accountNumber,
                "first_name": accountName,
                "last_name": accountLastName
            };

            fetch('https://api.paystack.co/customer/CUS_xnxdt6s1zg1f4nx/identification', {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${paystackApiKey}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(requestData)
            })
            .then(response => response.json())
            .then(data => {
                console.log('API Response:', data);

                alert('successfully: ' + data.status);
            })
            .catch(error => {
                console.error('Error:', error);
            });
        }
    </script>
</body>
</html>
